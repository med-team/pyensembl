Source: pyensembl
Section: science
Priority: optional
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>,
           Étienne Mollier <emollier@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-setuptools,
               python3-all,
               python3-pandas,
               python3-numpy,
               python3-datacache,
               python3-serializable,
               python3-memoized-property,
               python3-gtfparse,
               python3-pytest <!nocheck>,
#              python3-tinytimer # only run during tests that are not run because of
#              demand for internet access
Standards-Version: 4.7.0
Homepage: https://github.com/openvax/pyensembl
Vcs-Browser: https://salsa.debian.org/med-team/pyensembl
Vcs-Git: https://salsa.debian.org/med-team/pyensembl.git
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-pybuild

Package: pyensembl
Architecture: all
Depends:
 python3-pkg-resources,
 ${python3:Depends},
 ${misc:Depends},
Description: installs data from the Ensembl genome database
 The Ensembl genome database is an established reference
 for genomic sequences and their automated annotation.
 To have this data local has advantages for bulk analyses,
 e.g. for the mapping of reads from RNA-seq against the
 latest golden path - or a previous one to compare analyses.
 .
 This package provides a reproducible way to insatll this
 data and thus simplify the automation of respective
 workflows.
