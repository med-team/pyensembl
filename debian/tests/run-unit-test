#!/bin/bash
set -e

pkg=pyensembl
if [ "$AUTOPKGTEST_TMP" = "" ] ; then
  AUTOPKGTEST_TMP=`mktemp -d /tmp/${pkg}-test.XXXXXX`
  trap "rm -rf $AUTOPKGTEST_TMP" 0 INT QUIT ABRT PIPE TERM
fi

cp -a tests/ $AUTOPKGTEST_TMP

cd $AUTOPKGTEST_TMP

export HOME="$AUTOPKGTEST_TMP"

# common.py and data.py are missing from the source tarball in Pypi, but these
# files are present in the Github source code. These files are required to run
# the following tests:
rm tests/test_download_cache.py
rm tests/test_ensembl_gtf.py
rm tests/test_gene_ids.py
rm tests/test_gene_names.py
rm tests/test_gene_objects.py
rm tests/test_id_length.py
rm tests/test_missing_genome_sources.py
rm tests/test_mouse.py
rm tests/test_search.py
rm tests/test_sequence_data.py
rm tests/test_serialization.py
rm tests/test_transcript_ids.py
rm tests/test_transcript_objects.py
rm tests/test_ucsc_gtf.py

# pyensembl is missing a dependency needed to import test data. The test data
# could be added to debian/tests/data or in a multi-source tarball depending
# on the size of data being added. The following tests are affected:
rm tests/test_contigs.py
rm tests/test_exon_id.py
rm tests/test_exon_object.py
rm tests/test_string_representation.py
rm tests/test_timings.py
rm tests/test_transcript_sequences.py
rm tests/test_transcript_support_level.py

for py in $(py3versions -s)
do 
	$py -m pytest tests
done
